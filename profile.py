"""This profile instantiates a d840 machine running Ubuntu 20.04 64-bit LTS with 2 40Gbps NICs and allocates all existing disk space on it. The machine is connected to a Skylark FAROS massive MIMO system comprised of a FAROS hub, a Faros massive MIMO Base Station.  A second d740 machine is running the same base Ubuntu 20.04 image and is connected to a set of Iris UEs (clients).  Additionally, the image has Intel oneapi, Flexran, and all other compilation dependencies of Agora preinstalled as an image backed dataset.

Since this profile includes multiple disk images it will can take up to 30 minutes to instantiate

An optional MATLAB installation is useful to run experiments on FAROS with RENEWLab demos. 

Instructions:
Do all your work (compile / running) from the /scratch directory.
sudo chsh -s /bin/bash YOURUSERNAME

- For more information on Agora please refer to our [github](https://github.com/jianding17/Agora/wiki) and [wiki](https://github.com/jianding17/Agora) sites.
- For more information on RENEWLab, see [RENEW documentation page](https://wiki.renew-wireless.org/)

"""

import geni.portal as portal
import geni.urn as urn
import geni.rspec.pg as pg
import geni.rspec.emulab as elab
import geni.rspec.emulab.spectrum as spectrum

# Resource strings
PCIMG = 'urn:publicid:IDN+emulab.net+image+mww2023:renew-ubuntu-base'
#PCIMG = 'urn:publicid:IDN+emulab.net+image+argos-test:agora-2004-base'
MATLAB_DS_URN = 'urn:publicid:IDN+emulab.net:powdersandbox+imdataset+matlab2021ra-etc'
INTEL_LIBS_URN = 'urn:publicid:IDN+emulab.net:mww2023+imdataset+oneapi-flexran-dpdk'
#INTEL_LIBS_URN = 'urn:publicid:IDN+emulab.net:argos-test+imdataset+oneapi-flexran21'


MATLAB_MP = "/usr/local/MATLAB"
STARTUP_SCRIPT = "/local/repository/faros_start.sh"
PCHWBS = "d840"
PCHWUSER = "d740"
FAROSHWTYPE = "faros_sfp"
IRISHWTYPE = "iris030"
DEF_BS_SIZE = 0
DEF_BS_MOUNT_POINT = "/opt/data"
DEF_REMDS_MP = "/opt/data"

REMDS_TYPES = [("readonly", "Read Only"),
               ("rwclone", "Read-Write Clone (not persistent)"),
               ("readwrite", "Read-Write (persistent)")]

MMIMO_ARRAYS = ["", ("mmimo1-honors", "Honors rooftop array"),
                ("mmimo1-meb", "Meb basestation array"),
                ("mmimo1-ustar", "Ustar Basestation array (experimental)")]

UE = ["", ("iris1-mme1", "#1 Iris UE at MEB Rooftop Client Site 1"),
      ("iris2-mme1", "#2 Iris UE at MEB Rooftop Client Site 1"),
      ("iris1-mme2", "#1 Iris UE at MEB Rooftop Client Site 2"),
      ("iris2-mme2", "#2 Iris UE at MEB Rooftop Client Site 2")]



#
# Profile parameters.
#
pc = portal.Context()

# Frequency/spectrum parameters
pc.defineStructParameter(
    "freq_ranges", "Range", [],
    multiValue=True,
    min=1,
    multiValueTitle="Frequency ranges for over-the-air operation.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Min",
            portal.ParameterType.BANDWIDTH,
            3540.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Max",
            portal.ParameterType.BANDWIDTH,
            3550.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])

# Array to allocate
pc.defineStructParameter(
    "mmimo_devices", "mMIMO Devices", [],
    multiValue=True,
    min=0,
    multiValueTitle="Massive MIMO basestations to allocate.",
    members=[
        portal.Parameter(
            "mmimoid", "ID of Massive MIMO array to allocate.",
            portal.ParameterType.STRING, MMIMO_ARRAYS[0], MMIMO_ARRAYS
        ),
    ])

pc.defineStructParameter(
    "ue_devices", "UE Devices", [],
    multiValue=True,
    min=0,
    multiValueTitle="Iris UE clients to allocate.",
    members=[
        portal.Parameter(
            "ueid", "ID of Iris UE to allocate.",
            portal.ParameterType.STRING, UE[0], UE
        ),
    ])


# Allocate client radio?
pc.defineParameter("joinbsandues", "Attach the Iris client radios and base station array on same network interface",
                   portal.ParameterType.BOOLEAN, False)

pc.defineParameter("matlabds", "Attach the Matlab dataset to the compute host.",
                   portal.ParameterType.BOOLEAN, True)

pc.defineParameter("intellibs", "Attach the intel and 3rd party library dataset to the compute host.",
                   portal.ParameterType.BOOLEAN, True)

pc.defineParameter("intelmountpt", "Mountpoint for 3rd party libraries and inteloneAPI",
                   portal.ParameterType.STRING, "/opt")

pc.defineParameter("INTEL_LIBS_URN", "URN of the 3rd party library dataset", 
                   portal.ParameterType.STRING,
                   INTEL_LIBS_URN)

pc.defineParameter("hubints", "Number of interfaces to attach on hub (def: 2)",
                   portal.ParameterType.INTEGER, 2,
                   longDescription="This can be a number between 1 and 4.")

pc.defineParameter("fixedpc1id", "Fixed PC1 Node_id (Optional)",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Fix 'pc1' to this specific node.  Leave blank to allow for any available node of the correct type.")

pc.defineParameter("fixedpc2id", "Fixed PC2 Node_id (Optional)",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Fix 'pc2' to this specific node.  Leave blank to allow for any available node of the correct type.")

pc.defineParameter("remds", "Remote Dataset (Optional)",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Insert URN of a remote dataset to mount. Leave blank for no dataset.")

pc.defineParameter("remmp", "Remote Dataset Mount Point (Optional)",
                   portal.ParameterType.STRING, DEF_REMDS_MP, advanced=True,
                   longDescription="Mount point for optional remote dataset.  Ignored if the 'Remote Dataset' parameter is not set.")

pc.defineParameter("remtype", "Remote Dataset Mount Type (Optional)",
                   portal.ParameterType.STRING, REMDS_TYPES[0], REMDS_TYPES,
                   advanced=True, longDescription="Type of mount for remote dataset.  Ignored if the 'Remote Dataset' parameter is not set.")

# Bind and verify parameters.
params = pc.bindParameters()

for i, frange in enumerate(params.freq_ranges):
    if frange.freq_max - frange.freq_min < 1:
        perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)

if params.hubints < 1 or params.hubints > 4:
    perr = portal.ParameterError("Number of interfaces on hub to connect must be between 1 and 4 (inclusive).")
    portal.context.reportError(perr)

pc.verifyParameters()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# VNC - initialize
request.initVNC()

# Mount a remote dataset
def connect_DS(node, urn, mp, dsname = "", dstype = "rwclone"):
    if not dsname:
        dsname = "ds-%s" % node.name
    bs = request.RemoteBlockstore(dsname, mp)
    if dstype == "rwclone":
        bs.rwclone = True
    elif dstype == "readonly":
        bs.readonly = True
        
    # Set dataset URN
    bs.dataset = urn

    # Create link from node to OAI dataset rw clone
    bslink = request.Link("link_%s" % dsname, members=(node, bs.interface))
    bslink.vlan_tagging = True
    bslink.best_effort = True

# Request PC1
pc1 = request.RawPC("pc1")
pc1.startVNC()

if params.fixedpc1id:
    pc1.component_id=params.fixedpc1id
else:
    pc1.hardware_type = PCHWBS
pc1.disk_image = PCIMG

if params.intellibs:
    ilbspc1 = pc1.Blockstore( "intellibbspc1", params.intelmountpt )
    ilbspc1.dataset = params.INTEL_LIBS_URN
    #ilbspc1.size = "32GB"
    ilbspc1.placement = "sysvol"


if params.matlabds:
    mlbs = pc1.Blockstore( "matlabpc1", MATLAB_MP )
    mlbs.dataset = MATLAB_DS_URN
    mlbs.placement = "nonsysvol"

pc1.addService(pg.Execute(shell="sh", command="sudo chmod 775 /local/repository/faros_start.sh"))
pc1.addService(pg.Execute(shell="sh", command=STARTUP_SCRIPT))
if1pc1 = pc1.addInterface("if1pc1", pg.IPv4Address("192.168.1.1", "255.255.255.0"))
if1pc1.bandwidth = 40 * 1000 * 1000 # 40 Gbps
if1pc1.latency = 0
#Interface 2 - pclink
if2pc1 = pc1.addInterface("if2pc1", pg.IPv4Address("192.168.2.1", "255.255.255.0"))
#if2pc1.setJumboFrames()
#if2pc1.bandwidth = 40 * 1000 * 1000 # 40 Gbps

bss1 = pc1.Blockstore("pc1scratch","/scratch")
bss1.size = "500GB"
bss1.placement = "nonsysvol"
if params.remds:
    connect_DS(pc1, params.remds, params.remmp, dstype=params.remtype)

# Request PC2
pc2 = request.RawPC("pc2")
# pc2.component_id = "pc18-fort"
if params.fixedpc2id:
    pc2.component_id=params.fixedpc2id
else:
    pc2.hardware_type = PCHWUSER
pc2.disk_image = PCIMG
pc2.addService(pg.Execute(shell="sh", command="sudo chmod 775 /local/repository/faros_start.sh"))
if params.joinbsandues:
    pc2.addService(pg.Execute(shell="sh", command="/local/repository/faros_start.sh true"))
else:
    pc2.addService(pg.Execute(shell="sh", command=STARTUP_SCRIPT))
if1pc2 = pc2.addInterface("if1pc2", pg.IPv4Address("192.168.1.2", "255.255.255.0"))
if1pc2.latency = 0
#if1pc2.bandwidth = 10 * 1000 * 1000 # 10 Gbps for a d740 node
#Interface 2 - pclink
if2pc2 = pc2.addInterface("if2pc2", pg.IPv4Address("192.168.2.2", "255.255.255.0"))
#if2pc2.setJumboFrames()
#if2pc2.bandwidth = 10 * 1000 * 1000 # 10 Gbps for a d740 node
pc2.startVNC()

if params.intellibs:
    ilbspc2 = pc2.Blockstore( "intellibbspc2", params.intelmountpt )
    ilbspc2.dataset = params.INTEL_LIBS_URN
    #ilbspc2.size = "32GB"
    ilbspc2.placement = "sysvol"

bss2 = pc2.Blockstore("pc2scratch2","/scratch")
bss2.size = "200GB"
bss2.placement = "nonsysvol"

# link interfaces of the two PCs
pclink = request.Link("pclink", members=[if2pc1,if2pc2])
pclink.setJumboFrames()
# pclink.setNoBandwidthShaping()
# pclink.best_effort = True

# LAN connecting up everything (if needed).  Members are added below.
mmimolan = None
uelan = None

# Request a Faros BS.
if len(params.mmimo_devices):
    mmimolan = request.LAN("mmimolan")
    #mmimolan.best_effort = True
    mmimolan.latency = 0
    mmimolan.vlan_tagging = False
    mmimolan.setNoBandwidthShaping()
    #mmimolan.setNoInterSwitchLinks()
    mmimolan.addInterface(if1pc1)

# Request all Faros BSes requested
for i, mmimodev in enumerate(params.mmimo_devices):
    mm = request.RawPC("mm%d" % i)
    mm.component_id = mmimodev.mmimoid
    mm.hardware_type = FAROSHWTYPE
    for j in range(params.hubints):
        mmif = mm.addInterface()
        mmimolan.addInterface(mmif)

if len(params.ue_devices):
    if params.joinbsandues:
        uelan = mmimolan
    else:
        uelan = request.LAN("uelan")
        uelan.latency = 0
        uelan.vlan_tagging = False
        uelan.setNoBandwidthShaping()

    # Request all Faros BSes requested
    uelan.addInterface(if1pc2)
    for i, uedev in enumerate(params.ue_devices):
        ue = request.RawPC("ir%d" % i)
        ue.component_id = uedev.ueid
        ue.hardware_type = IRISHWTYPE
        ueif = ue.addInterface()
        uelan.addInterface(ueif)

# Add frequency request(s)
for frange in params.freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 100)

# Print the RSpec to the enclosing page.
pc.printRequestRSpec()
